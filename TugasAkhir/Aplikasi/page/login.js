import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";



export default function Login({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submit = () => {
        const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                console.log(user);
                navigation.navigate('Home', { email });
                // ...
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                // ..
            });

    }




    return (
        <View style={styles.container}>
            <View style={styles.headerBox}>
                <Text style={styles.textBox}>Food Recipes App</Text>
            </View>
            <Text style={styles.headertext}>Login </Text>
            <TextInput
                style={styles.textInput}
                placeholder='Email'
                value={email}
                onChangeText={(value) => setEmail(value)}
            />
            <TextInput
                style={styles.textInput}
                placeholder='Password'
                value={password}
                onChangeText={(value) => setPassword(value)}
                secureTextEntry
            />

            <TouchableOpacity style={styles.buttonLogin} onPress={submit} >
                <Text>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonRegister} onPress={() => navigation.navigate('Register')} >
                <Text>Register</Text>
            </TouchableOpacity>
            
        </View>
    )
}

const colors = {
    primary: "#F9E10C",
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
    },
    headerBox: {
        backgroundColor: colors.primary,
        marginTop: 10,
        width: 300,
        height: 81,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textBox: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    headerIcon: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    headertext: {
        fontSize: 24,
        marginVertical: 40,
        fontWeight: 'bold',
        color: 'black',
    },
    textInput: {
        width: 294,
        height: 48,
        borderWidth: 1,
        padding: 10,
        margin: 10,
        borderRadius: 10,
    },
    buttonRegister: {
        borderRadius: 10,
        marginTop: 10,
        width: 160,
        height: 50,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonLogin: {
        borderRadius: 10,
        marginVertical: 30,
        width: 160,
        height: 50,
        backgroundColor: colors.primary,
        justifyContent: "center",
        alignItems: "center",
    }


})