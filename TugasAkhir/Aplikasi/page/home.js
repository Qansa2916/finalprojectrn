import { getAuth } from "firebase/auth";
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Image, Text, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

export default function Home({ route, navigation }) {
    const { email } = route.params;

    const [data, setData] = useState();
    const getData = async () => {
        try {
            const res = await axios.get('https://www.themealdb.com/api/json/v1/1/search.php', {
                params: {
                    f: 'f',
                    api: 1,
                },
            })
            // console.log('res :', res.data);
            setData(res.data.meals)
        } catch (error) {
            alert(error.message)

        }

    };
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            {/* {console.log(data)} */}
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <Text style={styles.headertext}>Food Receips App</Text>
                    <Text style={styles.secText}>Let's get some food</Text>
                </View>
                <ScrollView>
                    {data && data.map((item, i) => {
                        return <>
                            <TouchableOpacity style={{ flex: 1 }} onPress={() => navigation.navigate('Detail', { data: item })}>
                                <View style={styles.boxItem}>
                                    <Image style={styles.foodPic} source={item.strMealThumb} />
                                    <Text style={styles.foodName}>{item.strMeal}</Text>
                                </View>
                            </TouchableOpacity>
                        </>
                    })}
                </ScrollView>

            </View>
        </>

    )
}

const colors = {
    primary: "#F9E10C",
    secondary: "#003366",
    grayBackground: "#EFEFEF",
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    boxHeader: {
        backgroundColor: colors.primary,
        height: 110,
        width: 360,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxItem: {
        height: 210,
        width: 200,
        alignItems: 'center',
        borderColor: 'grey',
        borderWidth: 1,
        marginVertical: 10,
        borderRadius: 10,
    },
    foodPic: {
        width: 200,
        height: 140,
        borderRadius: 10
    },
    headertext: {
        fontSize: 24,
        marginBottom: 10,
        fontWeight: 'bold',
        color: colors.secondary,
    },
    secText: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300'
    },
    foodName: {
        fontSize: 14,
        marginVertical: 10,
        color: 'black',
    },
    ingBox: {
        backgroundColor: colors.grayBackground,
        alignItems: 'center',
        justifyContent: 'space-around',
        borderRadius: 20,
        height: 250,
        width: 300,
        padding: 8,
        marginVertical: 10,
    },
    boxSosmed2: {
        flex: 1,
        height: 100,
        width: 250,
        alignItems: 'flex-start',
    },
    sosmed2: {
        flexDirection: 'row'
    },
    ingText: {
        color: colors.secondary,
        fontSize: 18,
        alignContent: 'center',
    }


})