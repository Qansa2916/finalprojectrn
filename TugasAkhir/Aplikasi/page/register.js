import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
import { TextInput } from 'react-native-gesture-handler';
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";

export default function Register({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const submit = () => {
    const Data = {
      email,
      password,
    };

    const auth = getAuth();
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log(user);
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });

  }


  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerBox}>
          <Text style={styles.textBox}>Food Recipes App</Text>
        </View>
        <Text style={styles.headertext}>Register</Text>
        <TextInput
          style={styles.textInput}
          placeholder='Email'
          value={email}
          onChangeText={(value) => setEmail(value)}
        />
        <TextInput
          style={styles.textInput}
          placeholder='Password'
          value={password}
          onChangeText={(value) => setPassword(value)}
          secureTextEntry
        />
        <TouchableOpacity style={styles.buttonLogin} onPress={submit} >
          <Text>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonRegister} onPress={() => navigation.navigate('Login')}>
          <Text>Login</Text>
        </TouchableOpacity>

      </View>
    </>
  )
}

const colors = {
  primary: "#F9E10C",
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    alignItems: 'center',
  },
  headerBox: {
    backgroundColor: colors.primary,
    marginTop: 10,
    width: 300,
    height: 81,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textBox: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  headerIcon: {
    height: 200,
    width: 200,
    resizeMode: 'contain',
  },
  headertext: {
    fontSize: 24,
    marginVertical: 40,
    fontWeight: 'bold',
    color: 'black',
  },
  textInput: {
    width: 294,
    height: 48,
    borderWidth: 1,
    padding: 10,
    margin: 10,
    borderRadius: 10,
  },
  buttonRegister: {
    borderRadius: 10,
    marginTop: 10,
    width: 160,
    height: 50,
    borderColor: 'black',
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonLogin: {
    borderRadius: 10,
    marginVertical: 30,
    width: 160,
    height: 50,
    backgroundColor: colors.primary,
    justifyContent: "center",
    alignItems: "center",
  }


})