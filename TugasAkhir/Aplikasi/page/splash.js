import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';


const Splash = ({ navigation }) => {
    let [fontsLoaded] = useFonts({
        'Montserrat-SemiBold': require('../asset/fonts/Montserrat-SemiBold.ttf'),
        'Montserrat-Light': require('../asset/fonts/Montserrat-Light.ttf')
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    return (
        <>

            <View style={styles.container} >
                <Text style={styles.textHeader}>Food Recipes App</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Image style={styles.logo} source={require("../asset/eco-food.png")} />
                </TouchableOpacity>
                <Text style={styles.textBody}>Make delicious World Food from your kitchen</Text>
            </View>


        </>

    )
};

export default Splash;



const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#F9E10C",
        alignItems: 'center',
        justifyContent: 'center',
    },
    textHeader: {
        fontSize: 24,
        fontWeight: '600',
        fontFamily: 'Montserrat-SemiBold',
        marginBottom: 72,
    },
    logo: {
        width: 129,
        height: 129,
    },
    textBody: {
        alignItems: 'center',
        fontFamily: 'Montserrat-light',
        marginTop: 70,
        fontSize: 24,
        fontWeight: '400',
    }
})