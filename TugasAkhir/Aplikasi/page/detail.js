import React from 'react'
import { StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';



export default function Detail({ route, navigation }) {
    const data = route.params.data;
    return (
        <>
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <Text style={styles.headertext}>Food Receips App</Text>
                    <Text style={styles.secText}>Let's get some food</Text>
                </View>
                <View style={styles.boxItem}>
                    <Image style={styles.foodPic} source={data.strMealThumb} />
                    <Text style={styles.foodName}>{data.strMeal}</Text>
                </View>
                <ScrollView>
                    <View style={styles.ingBox}>
                        <Text style={styles.ingText}>Ingredients</Text>
                        <View style={styles.boxInfo}>
                            <Text>{data.strIngredient1}</Text>
                            <Text>{data.strIngredient2}</Text>
                            <Text>{data.strIngredient3}</Text>
                            <Text>{data.strIngredient4}</Text>
                            <Text>{data.strIngredient5}</Text>
                            <Text>{data.strIngredient6}</Text>
                            <Text>{data.strIngredient7}</Text>
                            <Text>{data.strIngredient8}</Text>
                            <Text>{data.strIngredient9}</Text>
                            <Text>{data.strIngredient10}</Text>
                        </View>
                    </View>
                    <View style={styles.ingBox}>
                        <Text style={styles.ingText}>Intructions</Text>
                        <View style={styles.boxInfo}>
                            <Text >{data.strInstructions}</Text>
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity style={styles.buttonAbout} onPress={() => navigation.navigate('About')} >
                    <Text>To About Page</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

const colors = {
    primary: "#F9E10C",
    secondary: "#003366",
    grayBackground: "#EFEFEF",
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    boxHeader: {
        backgroundColor: colors.primary,
        height: 110,
        width: 360,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxItem: {
        height: 210,
        width: 200,
        alignItems: 'center',
        borderColor: 'grey',
        borderWidth: 1,
        marginVertical: 10,
        borderRadius: 10,
    },
    foodPic: {
        width: 200,
        height: 140,
        borderRadius: 10
    },
    headertext: {
        fontSize: 24,
        marginBottom: 10,
        fontWeight: 'bold',
        color: colors.secondary,
    },
    secText: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300'
    },
    foodName: {
        fontSize: 14,
        marginVertical: 10,
        color: 'black',
    },
    ingBox: {
        backgroundColor: colors.grayBackground,
        alignItems: 'center',
        justifyContent: 'space-around',
        borderRadius: 20,
        height: 'auto',
        width: 300,
        padding: 8,
        marginVertical: 10,
    },
    boxInfo: {
        width: 250,
        alignItems: 'flex-start',
    },
    sosmed2: {
        flexDirection: 'row'
    },
    ingText: {
        color: colors.secondary,
        fontSize: 18,
        alignContent: 'center',
    },
    buttonAbout: {
        borderRadius: 10,
        marginVertical: 30,
        width: 160,
        height: 50,
        backgroundColor: colors.primary,
        justifyContent: "center",
        alignItems: "center",
    }


})