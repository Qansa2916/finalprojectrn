import React from 'react'
import { StyleSheet, Text, View ,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

export default function About({navigation}) {
    return (
        <View style={styles.container}>
            <View style={styles.boxHeader}>
                <Text style={styles.headertext}>About Me</Text>
            </View>
            <TouchableOpacity style={styles.buttonHome} onPress={() => navigation.navigate('Login')} >
                <Text>Logout</Text>
            </TouchableOpacity>
            <View style={styles.boxNama}>
                <View>
                    <Text style={styles.profileText}>Qansa Alghifari</Text>
                    <Text style={styles.profileText2}>React Native Developer</Text>
                </View>
                <Ionicons style={styles.iconProfile} name="person-circle-outline" color={colors.primary} size={80} />
            </View>
            <View style={styles.boxProfile}>
                <View>
                    <Text style={styles.portofolioText}>Portofolio</Text>
                </View>
                <View style={styles.boxSosmed}>
                    <View style={styles.sosmed}>
                        <FontAwesome5 name="gitlab" size={40} color={colors.primary} />
                        <Text>@Qansa2916</Text>
                    </View>
                    <View style={styles.sosmed}>
                        <FontAwesome name="github" size={40} color={colors.primary} />
                        <Text>@Qansa2916</Text>
                    </View>
                </View>
            </View>
            <View style={styles.boxProfile2}>
                <Text style={styles.portofolioText}>Contact Me</Text>
                <View style={styles.boxSosmed2}>
                    <View style={styles.sosmed2}>
                        <FontAwesome name="facebook-square" size={24} color={colors.primary} />
                        <Text>@Qansa2916</Text>
                    </View>
                    <View style={styles.sosmed2}>
                        <FontAwesome name="instagram" size={24} color={colors.primary} />
                        <Text>@Qansa2916</Text>
                    </View>
                    <View style={styles.sosmed2}>
                        <FontAwesome name="twitter" size={24} color={colors.primary} />
                        <Text>@Qansa2916</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const colors = {
    primary: "#F9E10C",
    secondary: "#003366",
    grayBackground: "#EFEFEF",
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    boxHeader: {
        backgroundColor: colors.primary,
        height: 110,
        width: 360,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxNama: {
        height: 110,
        width: 360,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        paddingHorizontal: 20,

    },
    headertext: {
        fontSize: 24,
        marginBottom: 10,
        fontWeight: 'bold',
        color: colors.secondary,
    },
    profileText: {
        fontSize: 20,
        marginVertical: 10,
        fontWeight: 'bold',
        color: 'black',
    },
    profileText2: {
        fontSize: 14,
        marginVertical: 10,
        color: 'black',
    },
    boxProfile: {
        backgroundColor: colors.grayBackground,
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        borderRadius: 20,
        height: 120,
        width: 360,
        padding: 8,
        marginVertical: 10,
    },
    boxProfile2: {
        backgroundColor: colors.grayBackground,
        alignItems: 'center',
        justifyContent: 'space-around',
        borderRadius: 20,
        height: 250,
        width: 360,
        padding: 8,
        marginVertical: 10,
    },
    boxSosmed: {
        flex: 1,
        flexDirection: 'row',
        width: 360,
        justifyContent: 'space-around',
    },
    boxSosmed2: {
        flex: 1,
        justifyContent: 'space-around',
        marginVertical: 5,
        height: 100,
        width: 350,
    },
    sosmed: {
        alignItems: 'center',
    },
    sosmed2: {
        flexDirection: 'row'
    },
    portofolioText: {
        color: colors.secondary,
        fontSize: 18,
        alignContent: 'center',
    },
    buttonHome: {
        borderRadius: 10,
        marginVertical: 30,
        width: 160,
        height: 50,
        backgroundColor: colors.primary,
        justifyContent: "center",
        alignItems: "center",
    }


})