import { StatusBar, Text } from 'react-native';
import react from 'react';
import Aplikasi from './Aplikasi';
// Import the functions you need from the SDKs you need
import { getApps, initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBcSnOX-6ZdOvSe8CI1YloM0E0Z0qtemSI",
  authDomain: "authrn-edc39.firebaseapp.com",
  projectId: "authrn-edc39",
  storageBucket: "authrn-edc39.appspot.com",
  messagingSenderId: "896299733399",
  appId: "1:896299733399:web:689280ce2a3e1d875034d2"
};
if (!getApps.length) {
  initializeApp(firebaseConfig);
  // console.log(getApps());
}


export default function App() {

  return (
    <>
      <StatusBar translucent={false} />
      <Aplikasi />
      
    </>
  );
}

